/*
  Software License Agreement (BSD License)
  Copyright (c) 2018, Fredrik Macintosh
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:
    Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
    Redistributions in binary form must reproduce the above
  copyright notice, this list of conditions and the following
  disclaimer in the documentation and/or other materials provided
  with the distribution.
    Neither the name of imu_client nor the names of its
  contributors may be used to endorse or promote products derived
  from this software without specific prior written permission.
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  POSSIBILITY OF SUCH DAMAGE.
  author: Fredrik Macintosh
*/

/*
   This intend to connect to a Wifi Access Point
   and a rosserial socket server.
   You can launch the rosserial socket server with
   rosrun rosserial_python serial_node.py tcp
   The default port is 11411

*/
#include <ESP8266WiFi.h>
#include <ros.h>
#include <geometry_msgs/Vector3Stamped.h>
#include <Wire.h>

#define LED D0
#define BRIGHT    250     //max led intensity (1-500)
#define INHALE    750    //Inhalation time in milliseconds.
#define PULSE     INHALE*1000/BRIGHT

// Select SDA and SCL pins for I2C communication
const uint8_t scl = D6;
const uint8_t sda = D5;


// Sensitivity scaling according to documentation
const uint16_t AccelScaleFactor = 16384;
const uint16_t GyroScaleFactor = 131;

//Calibration ting
float offsetAccelX = 0.0;
float offsetAccelY = 0.0;
float offsetGyroZ = 0.0;

// MPU6050 Slave Device Address
const uint8_t MPU6050SlaveAddress = 0x68;

// MPU6050 few configuration register addresses
const uint8_t MPU6050_REGISTER_SMPLRT_DIV   =  0x19;
const uint8_t MPU6050_REGISTER_USER_CTRL    =  0x6A;
const uint8_t MPU6050_REGISTER_PWR_MGMT_1   =  0x6B;
const uint8_t MPU6050_REGISTER_PWR_MGMT_2   =  0x6C;
const uint8_t MPU6050_REGISTER_CONFIG       =  0x1A;
const uint8_t MPU6050_REGISTER_GYRO_CONFIG  =  0x1B;
const uint8_t MPU6050_REGISTER_ACCEL_CONFIG =  0x1C;
const uint8_t MPU6050_REGISTER_FIFO_EN      =  0x23;
const uint8_t MPU6050_REGISTER_INT_ENABLE   =  0x38;
const uint8_t MPU6050_REGISTER_ACCEL_XOUT_H =  0x3B;
const uint8_t MPU6050_REGISTER_SIGNAL_PATH_RESET  = 0x68;

int16_t AccelX, AccelY, AccelZ, Temperature, GyroX, GyroY, GyroZ;



const char* ssid     = "robustrc";
const char* password = "robustrc";

bool runOnce = true;

// Set the rosserial socket server IP address (Simply the IP of the host computer running roscore)
IPAddress server(192, 168, 1, 5);

// Set the rosserial socket server port (Standard communication port for rosserial)
const uint16_t serverPort = 11411;

ros::NodeHandle nh;
geometry_msgs::Vector3Stamped vec3_msg;
ros::Publisher imuPublisher("imuData0", &vec3_msg);


void setup()
{
  pinMode(LED, OUTPUT);   // LED pin as output.
  //Serial.begin(115200);
 //Wire.begin();
 Wire.begin(sda,scl);

  MPU6050_Init();
  calibrateMPU6050();
  // Connect the ESP8266 the the wifi AP using the information provided above.
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED)
  {
    for (int i = 1; i < BRIGHT; i++) {
      digitalWrite(LED, LOW);          // turn the LED on.
      delayMicroseconds(i * 10);       // wait
      digitalWrite(LED, HIGH);         // turn the LED off.
      delayMicroseconds(PULSE - i * 10); // wait
      delay(0);                        //to prevent watchdog firing.
    }
    //ramp decreasing intensity, Exhalation (half time):
    for (int i = BRIGHT - 1; i > 0; i--) {
      digitalWrite(LED, LOW);          // turn the LED on.
      delayMicroseconds(i * 10);        // wait
      digitalWrite(LED, HIGH);         // turn the LED off.
      delayMicroseconds(PULSE - i * 10); // wait
      i--;
      delay(0);                        //to prevent watchdog firing.
    }
  }
  // Set the connection to rosserial socket server and start the node.
  nh.getHardware()->setConnection(server, serverPort);
  nh.initNode();

  // Start publisher
  nh.advertise(imuPublisher);
}

void loop()
{

  if (!nh.connected())
  {
    digitalWrite(LED, LOW);
  }
  
  Read_RawValue(MPU6050SlaveAddress, MPU6050_REGISTER_ACCEL_XOUT_H);
  vec3_msg.vector.x = (float)AccelX / AccelScaleFactor  - offsetAccelX;     // Accelerometer x
  vec3_msg.vector.y = (float)AccelY / AccelScaleFactor - offsetAccelY; // Accelerometer y
  vec3_msg.vector.z = (float)GyroZ / GyroScaleFactor - offsetGyroZ;  // Yaw rate
  vec3_msg.header.stamp = nh.now();
  vec3_msg.header.frame_id = "0";
  
  if (nh.connected())
  {
    // Send the content.
    imuPublisher.publish(&vec3_msg);
    digitalWrite(LED, HIGH);
  }

  nh.spinOnce();
  
}


// read all 14 register WON'T NEED ALL OF THESE.
// ALSO, AGAIN, NO CLUE WHAT THE HECK IS GOING ON. Address hendling is scary.
void Read_RawValue(uint8_t deviceAddress, uint8_t regAddress)
{
  Wire.beginTransmission(deviceAddress);
  Wire.write(regAddress);
  Wire.endTransmission();
  Wire.requestFrom(deviceAddress, (uint8_t)14);
  AccelX = (((int16_t)Wire.read() << 8) | Wire.read());
  AccelY = (((int16_t)Wire.read() << 8) | Wire.read());
  AccelZ = (((int16_t)Wire.read() << 8) | Wire.read());
  Temperature = (((int16_t)Wire.read() << 8) | Wire.read());
  GyroX = (((int16_t)Wire.read() << 8) | Wire.read());
  GyroY = (((int16_t)Wire.read() << 8) | Wire.read());
  GyroZ = (((int16_t)Wire.read() << 8) | Wire.read());
}

void I2C_Write(uint8_t deviceAddress, uint8_t regAddress, uint8_t data)
{
  Wire.beginTransmission(deviceAddress);
  Wire.write(regAddress);
  Wire.write(data);
  Wire.endTransmission();
}

//configure MPU6050
void MPU6050_Init()
{
  delay(200);
  I2C_Write(MPU6050SlaveAddress, MPU6050_REGISTER_SMPLRT_DIV, 0x07);
  I2C_Write(MPU6050SlaveAddress, MPU6050_REGISTER_PWR_MGMT_1, 0x01);
  I2C_Write(MPU6050SlaveAddress, MPU6050_REGISTER_PWR_MGMT_2, 0x00);
  I2C_Write(MPU6050SlaveAddress, MPU6050_REGISTER_CONFIG, 0x00);
  I2C_Write(MPU6050SlaveAddress, MPU6050_REGISTER_GYRO_CONFIG, 0x00);//set +/-250 degree/second full scale
  I2C_Write(MPU6050SlaveAddress, MPU6050_REGISTER_ACCEL_CONFIG, 0x00);// set +/- 2g full scale
  I2C_Write(MPU6050SlaveAddress, MPU6050_REGISTER_FIFO_EN, 0x00);
  I2C_Write(MPU6050SlaveAddress, MPU6050_REGISTER_INT_ENABLE, 0x01);
  I2C_Write(MPU6050SlaveAddress, MPU6050_REGISTER_SIGNAL_PATH_RESET, 0x00);
  I2C_Write(MPU6050SlaveAddress, MPU6050_REGISTER_USER_CTRL, 0x00);
  delay(200);
}

//Calibrate MPU6050 (Remove small offsets)
void calibrateMPU6050()
{
  for (size_t i = 0; i < 500; i++)
  {
    Read_RawValue(MPU6050SlaveAddress, MPU6050_REGISTER_ACCEL_XOUT_H);
    offsetAccelX += (float)AccelX / AccelScaleFactor; // Accelerometer x
    offsetAccelY += (float)AccelY / AccelScaleFactor; // Accelerometer y
    offsetGyroZ += (float)GyroZ / GyroScaleFactor;
  }
  offsetAccelX = offsetAccelX / 500.0;
  offsetAccelY = offsetAccelY / 500.0;
  offsetGyroZ = offsetGyroZ / 500.0;
}


