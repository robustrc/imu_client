
# What is this repository for? #

* Publishing IMU data from a NodeMCU ESP8266 12E to a rostopic.


# How do I get set up? #

## Requirements ##

* NodeMCU ES8266-12E Development Kit, it is possible that the code will run on any ESP8266 board, but this is completely untested.
* ESP8266 library for arduino.
* Ros library for arduino. (See rosserial_arduino)
* Additional board manager: Preferences > Additional Board Manager URLs > http://arduino.esp8266.com/stable/package_esp8266com_index.json

## Running ##

* Change IP and SSID etc as needed in the source code.
* Flash the code to your NodeMCU board using USB cable (or any other method you know works I suppose).
* On your target computer, run 
>rosrun rosserial_python serial_node.py tcp

 ### Published Topic ###
 
> /car#ImuData

Where # is replaced by the numeric ID of the car the board is mounted to (This is decided by you the user)

Done! Do as you please with the data on the topic.

# Who do I talk to? #
	
* Fredrik Macintosh
* fredrik.macintosh@gmail.com
